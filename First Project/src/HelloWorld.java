
public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String ausgabe = "Die Sonne scheint hell";  //Deklaration und Initialisierung der Variable "ausgabe"
		String bericht = "Wetterbericht von heute:";
		String aussage = "Die Zahlen sind: ";
		
		int zahl1, zahl2; //Deklaration der ganzzahligen Variable "zahl" (Datentyp: integer)
		double kommaZahl; //double f�r Kommazahlen

		
		zahl1 = 5;	//Initialisierung der Variable "zahl" mit dem Wert 5 2
		zahl2 = 10;
		kommaZahl = 6.95788712871286412906126902;
		
		System.out.print("Hallo Welt\n");
		System.out.println("Hallo Welt");
		System.out.println(bericht);
		System.out.println(ausgabe);
		System.out.println(zahl1);
		System.out.println(aussage + zahl1 + " und" + zahl2);
		System.out.println(kommaZahl);
		
		//printf-Befehl, um Ausgabe auf 3 Nachkommastellen zu reduzieren
		System.out.printf("Unsere Kommazahl: %.3f", kommaZahl);  
	}

}
